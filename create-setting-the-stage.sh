#!/usr/bin/env bash
readonly repo=setting-the-stage

rm -rf $repo # delete previous repo
mkdir $repo && cd $repo
git init

# commit 1 - intellij project
cp -r ../src/empty-intellij-project/. .
git add -f .idea/*
git add .gitignore
git ci -m "add empty intellij project"

# commit 2
touch documentation.txt
cat ../src/math/mul-doc > documentation.txt
touch code.py
cat ../src/math/mul > code.py
git add -A && git ci -m "add multiply function"

# commit 3 (not committed!)
touch author.txt
echo "nero vanbiervliet" >> author.txt

cat ../src/math/all-doc > documentation.txt
cat ../src/math/all > code.py

# zipping
cd ..
# (q)uiet, (F)ile (Sync) instead of update
zip -rqFS $repo.zip $repo

# put zip in repo TODO " necessary?
mkdir "$repo-zip"
cp "$repo.zip" "$repo-zip"
cd "$repo-zip"
git init
git add -A
git ci -m "add zip"
