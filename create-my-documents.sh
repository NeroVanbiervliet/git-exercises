#!/usr/bin/env bash
readonly repo=my-documents

rm -rf $repo # delete previous repo
mkdir $repo && cd $repo
git init

# commit 1 - intellij project
cp -r ../src/empty-intellij-project/.gitignore .
git add -A
git ci -m "add .gitignore"

# add gitignore
touch .gitignore
cat ../src/gitignore-images >> .gitignore
git add -A && git ci -m "edit gitignore for images"

# commit 1
mkdir emails
cp -r ../src/emails .
git add -A && git ci -m "add emails"

# create br email-backup
git co -b email-backup
git co master
rm emails/emma.txt
git add -A  && git ci -m "delete embarrassing email"

# commit 2
touch password.txt
echo "Neroo-123" >> password.txt
git add -A && git ci -m "add password"

# create br password-experiment
git co -b password-experiment
echo "Vanbiervliet-123" > password.txt
git add -A && git ci -m "change to safer password"
git co master

# commit 3
touch business-card.txt cv.pdf
cat ../src/business/professional >> business-card.txt
cp ../src/cv/apple.pdf cv.pdf
git add -A && git ci -m "add business card and cv"

# commit 4
echo "Nero-123" > password.txt
git add -A && git ci -m "fix typo in password"

# commmit 5
touch automate-job.py
cat ../src/automate/init.py > automate-job.py
git add -A && git ci -m "add script to automate job"

# create br personal-info
git co -b personal-info
cat ../src/business/personal >> business-card.txt
git add -A && git ci -m "add personal info to business card"
git co master

# commit 6
rm emails/*
git add -A && git ci -m "remove emails"

# create br cv-with-email
git co -b cv-with-email
cp ../src/cv/with-email.pdf cv.pdf
git add -A && git ci -m "add email to cv"
git co master

# create br automate-wait
git co -b automate-wait
cat ../src/automate/with-wait.py > automate-job.py
git add -A && git ci -m "add random waiting time to job automation"
git co master

# commit 7
cat ../src/automate/with-content.py > automate-job.py
git add -A && git ci -m "add random content to job automation"

# commit 8
cat ../src/business/professional-update > business-card.txt
cp ../src/cv/orange.pdf cv.pdf
git add -A && git ci -m "update business card and cv because of job change"

# create br old-version
git co -b old-version
git reset --hard HEAD~2 # set two commits behind
git co master
