#!/usr/bin/env bash
readonly repo=adam-and-eve

rm -rf $repo # delete previous repo
mkdir $repo && cd $repo
git init

# commit 1 - intellij project
# NEED steek deze commit in een apart script
cp -r ../src/empty-intellij-project/.gitignore .
git add -A
git ci -m "add .gitignore"

# commit 2
touch people.txt
echo "adam" >> people.txt
touch planets.txt
echo "earth" >> planets.txt
git add -A && git ci -m "create world and first man"

# commit 3
echo "eve" >> people.txt
git add -A && git ci -m "create first woman"

# commit 4
touch forbidden-fruits.txt
echo "apple" >> forbidden-fruits.txt
echo "mango" >> forbidden-fruits.txt
git add -A && git ci -m "add forbidden fruits"

# commit 5
echo "cain" >> people.txt
echo "able" >> people.txt
git add -A && git ci -m "give birth"

# commit 6
touch secret-paper.txt
echo "Gott ist tot - Nietzsche" >> secret-paper.txt
git add -A && git ci -m "find secret paper message"

# commit 7
rm secret-paper.txt
git add -A && git ci -m "burn paper"

# commit 8
cat ../src/other-planets >> planets.txt
git add -A && git ci -m "add missing planets"

# commit 9
head -n -1 planets.txt > temp ; mv temp planets.txt
git add -A && git ci -m  "remove pluto"


